function FindProxyForURL(url, host) {
    
    if (shExpMatch(host, "*.slack-*.com")) {        
        return "SOCKS 127.0.0.1:8000; DIRECT";
    }
    
    return "DIRECT";
}
